package com.example.joabsilva.projetoapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView location, user, user_add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        user = (ImageView) findViewById(R.id.imageView3);
        user_add = (ImageView) findViewById(R.id.imageView6);
        location = (ImageView) findViewById(R.id.imageView5);

       user.setOnClickListener(new View.OnClickListener(){
            @Override
           public void  onClick(View v){
                Intent i = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(i);
            }
        });
        user_add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void  onClick(View v){
                Intent i = new Intent(MainActivity.this, Main3Activity.class);
                startActivity(i);
            }
        });
        location.setOnClickListener(new View.OnClickListener(){
            @Override
            public void  onClick(View v){
                Intent i = new Intent(MainActivity.this, Main4Activity.class);
                startActivity(i);
            }
        });
    }
}

